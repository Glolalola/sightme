package hr.asc.snjezana.sightme.handlers;

import android.app.Activity;

import java.io.Serializable;

import hr.asc.snjezana.sightme.prompts.LoadingPrompt;
import hr.asc.snjezana.sightme.webservice.ServiceResponseHandler;


/**
 * Created by maja on 29.04.16..
 */
public abstract class ResponseHandler implements ServiceResponseHandler {

    private Activity activity;
    private Object[] args;
    private LoadingPrompt loadingPrompt;

    public ResponseHandler(Activity activity, Serializable... args) {
        this.activity = activity;
        this.args = args;
        this.loadingPrompt = new LoadingPrompt(this.activity);
    }

    @Override
    public void onPreSend() {
        loadingPrompt.showPrompt();
    }

    @Override
    public void onPostSend() {
        loadingPrompt.hidePrompt();
    }

    public Activity getActivity() {
        return activity;
    }

    public Object[] getArgs() {
        return args;
    }
}
