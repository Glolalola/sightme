package hr.asc.snjezana.sightme.model;

import java.io.Serializable;

/**
 * Created by maja on 29.04.16..
 */
public class Credentials implements Serializable {
    String username;
    String password;

    public Credentials() {
    }

    public Credentials(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return this.username + " " + this.password;
    }
}
