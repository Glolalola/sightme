package hr.asc.snjezana.sightme.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import hr.asc.snjezana.sightme.R;
import hr.asc.snjezana.sightme.RequestActivity;
import hr.asc.snjezana.sightme.adapters.RequestAdapter;
import hr.asc.snjezana.sightme.core.SessionManager;
import hr.asc.snjezana.sightme.model.Person;
import hr.asc.snjezana.sightme.model.Request;
import hr.asc.snjezana.sightme.webservice.ServiceAsyncTask;
import hr.asc.snjezana.sightme.webservice.ServiceCaller;
import hr.asc.snjezana.sightme.webservice.ServiceParams;
import hr.asc.snjezana.sightme.webservice.ServiceResponse;
import hr.asc.snjezana.sightme.webservice.SimpleResponseHandler;


public class RequestFragment extends Fragment {


    RequestAdapter adapter;
    ListView requests;
    ArrayList<Request> list;
    Person client;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_request, container, false);
        requests = (ListView) v.findViewById(R.id.request_list);
        client = SessionManager.getInstance(getContext()).retrieveSession(SessionManager.PERSON_INFO_KEY, Person.class);
        new ServiceAsyncTask(updater).execute(new ServiceParams(getString(R.string.request_person_path), ServiceCaller.HTTP_GET, client));

        return v;
    }

    @Override
    public void onResume() {
        new ServiceAsyncTask(updater).execute(new ServiceParams(getString(R.string.request_person_path), ServiceCaller.HTTP_GET, client));
        super.onResume();
    }

    SimpleResponseHandler updater = new SimpleResponseHandler() {
        @Override
        public boolean handleResponse(ServiceResponse response) {
            Type listType = new TypeToken<ArrayList<Request>>(){}.getType();
            list = new Gson().fromJson(response.getJsonResponse(), listType);
            if(list != null) {
                adapter = new RequestAdapter(getContext(), R.layout.list_item_request, list);
                requests.setAdapter(adapter);
                requests.setOnItemClickListener(onItemClickListener);
                return true;
            }
            return false;
        }
    };


    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Request r = (Request) requests.getItemAtPosition(position);

            Intent intent = new Intent(getContext(), RequestActivity.class);
            intent.putExtra("request", new Gson().toJson(r));
            startActivity(intent);
        }
    };
}
