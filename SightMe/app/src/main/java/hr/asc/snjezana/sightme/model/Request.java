package hr.asc.snjezana.sightme.model;

import java.io.Serializable;

/**
 * Created by maja on 26.03.16..
 */
public class Request implements Serializable {

    long idRequest;
    String price;
    String description;
    String requestDate;
    String requestTime;
    boolean requestDeleted;
    Person guide;
    Person tourist;
    RequestStatus guideStatus;
    RequestStatus touristStatus;

    public Request() {
    }

    public Request(long idRequest, String price, String description, String requestDate, String requestTime, boolean requestDeleted, Person guide, Person tourist, RequestStatus guideStatus, RequestStatus touristStatus) {
        this.idRequest = idRequest;
        this.price = price;
        this.description = description;
        this.requestDate = requestDate;
        this.requestTime = requestTime;
        this.requestDeleted = requestDeleted;
        this.guide = guide;
        this.tourist = tourist;
        this.guideStatus = guideStatus;
        this.touristStatus = touristStatus;
    }

    public long getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(long idRequest) {
        this.idRequest = idRequest;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public boolean isRequestDeleted() {
        return requestDeleted;
    }

    public void setRequestDeleted(boolean requestDeleted) {
        this.requestDeleted = requestDeleted;
    }

    public Person getGuide() {
        return guide;
    }

    public void setGuide(Person guide) {
        this.guide = guide;
    }

    public Person getTourist() {
        return tourist;
    }

    public void setTourist(Person tourist) {
        this.tourist = tourist;
    }

    public RequestStatus getGuideStatus() {
        return guideStatus;
    }

    public void setGuideStatus(RequestStatus guideStatus) {
        this.guideStatus = guideStatus;
    }

    public RequestStatus getTouristStatus() {
        return touristStatus;
    }

    public void setTouristStatus(RequestStatus touristStatus) {
        this.touristStatus = touristStatus;
    }

    @Override
    public String toString() {
        return "Request{" +
                "idRequest=" + idRequest +
                ", price='" + price + '\'' +
                ", description='" + description + '\'' +
                ", requestDate='" + requestDate + '\'' +
                ", requestTime='" + requestTime + '\'' +
                ", requestDeleted=" + requestDeleted +
                ", guide=" + guide +
                ", tourist=" + tourist +
                ", guideStatus=" + guideStatus +
                ", touristStatus=" + touristStatus +
                '}';
    }
}
