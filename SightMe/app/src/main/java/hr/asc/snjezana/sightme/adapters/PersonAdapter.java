package hr.asc.snjezana.sightme.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import hr.asc.snjezana.sightme.ImageConverter;
import hr.asc.snjezana.sightme.R;
import hr.asc.snjezana.sightme.core.Logger;
import hr.asc.snjezana.sightme.model.Person;

/**
 * Created by maja on 26.03.16..
 */
public class PersonAdapter extends ListAdapter<Person> {
    Context context;
    public PersonAdapter(Context context, int resource, ArrayList<Person> items) {
        super(context, resource, items);
        this.context = context;
    }

    public static class ViewHolder {
        public CircleImageView picture;
        public TextView personName;
        public TextView personSurname;
        public TextView birthday;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;
        try {
            if (convertView == null) {
                vi = getInflater().inflate(R.layout.list_item_person, null);
                holder = new ViewHolder();
                holder.picture = (CircleImageView) vi.findViewById(R.id.itemPersonPicture);
                holder.personName = (TextView) vi.findViewById(R.id.itemPersonName);
                holder.personSurname = (TextView) vi.findViewById(R.id.itemPersonSurname);
                holder.birthday = (TextView) vi.findViewById(R.id.itemPersonBirthday);

                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }

            Person current = getItems().get(position);
            holder.personName.setText(current.getName());
            holder.birthday.setText(current.getBirthday());
            holder.personSurname.setText(current.getSurname());
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.profile);
            Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 100);
            if (current.getPicture()==null){
                holder.picture.setImageBitmap(circularBitmap);
            }
        } catch (Exception e) {
            Logger.log("Failed to fill view with users", getClass().getName(), Log.ERROR);
        }
        return vi;
    }

}

