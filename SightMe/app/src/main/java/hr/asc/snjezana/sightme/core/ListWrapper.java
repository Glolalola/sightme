package hr.asc.snjezana.sightme.core;

import java.io.Serializable;
import java.util.ArrayList;

import hr.asc.snjezana.sightme.model.Person;

/**
 * Created by Glo on 13.5.2016..
 */
public class ListWrapper<T> implements Serializable {

    private ArrayList<T> list;

    public ListWrapper(ArrayList<T> list) {
        this.list = list;
    }

    public ArrayList<T> getList() {
        return list;
    }
}
