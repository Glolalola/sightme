package hr.asc.snjezana.sightme.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TwoLineListItem;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import hr.asc.snjezana.sightme.ChatActivity;
import hr.asc.snjezana.sightme.ProfileActivity;
import hr.asc.snjezana.sightme.R;
import hr.asc.snjezana.sightme.adapters.PersonAdapter;
import hr.asc.snjezana.sightme.core.ListWrapper;
import hr.asc.snjezana.sightme.core.SessionManager;
import hr.asc.snjezana.sightme.model.Messages;
import hr.asc.snjezana.sightme.model.Person;
import hr.asc.snjezana.sightme.prompts.LoadingPrompt;
import hr.asc.snjezana.sightme.webservice.ServiceAsyncTask;
import hr.asc.snjezana.sightme.webservice.ServiceCaller;
import hr.asc.snjezana.sightme.webservice.ServiceParams;
import hr.asc.snjezana.sightme.webservice.ServiceResponse;
import hr.asc.snjezana.sightme.webservice.SimpleResponseHandler;


public class ContactsFragment extends Fragment {


    PersonAdapter adapter;
    ListView users;
    ArrayList<Person> list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_contacts, container, false);
        users = (ListView) v.findViewById(R.id.contactList);

        long id = SessionManager.getInstance(getContext()).retrieveSession(SessionManager.PERSON_INFO_KEY, Person.class).getIdPerson();
        new ServiceAsyncTask(updater).execute(new ServiceParams(getString(R.string.message_contacts_path) + id, ServiceCaller.HTTP_GET, null));
        return v;
    }

    SimpleResponseHandler updater = new SimpleResponseHandler() {
        @Override
        public boolean handleResponse(ServiceResponse response) {
            Type listType = new TypeToken<ArrayList<Person>>(){}.getType();
            list = new Gson().fromJson(response.getJsonResponse(), listType);
            if(list != null) {
                adapter = new PersonAdapter(getActivity(), R.layout.fragment_contacts, list);
                users.setAdapter(adapter);
                users.setOnItemClickListener(onItemClickListener);
                return true;
            }
            return false;
        }
    };

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final Person guide = (Person) users.getItemAtPosition(position);
            Person client = SessionManager.getInstance(getContext()).retrieveSession(SessionManager.PERSON_INFO_KEY, Person.class);
            final LoadingPrompt prompt = new LoadingPrompt(getContext());
            prompt.showPrompt();
            new ServiceAsyncTask(new SimpleResponseHandler() {
                @Override
                public boolean handleResponse(ServiceResponse response) {
                    prompt.hidePrompt();
                    if(response.getHttpCode() == 200) {

                        Type listType = new TypeToken<ArrayList<Messages>>(){}.getType();
                        ArrayList<Messages> messageList = new Gson().fromJson(response.getJsonResponse(), listType);

                        Log.i("handler", String.valueOf(messageList));
                        if(messageList != null){

                            startActivity(new Intent(getContext(), ChatActivity.class).putExtra("messageList",
                                    new ListWrapper<>(messageList)).putExtra("personId", guide.getIdPerson())
                                    .putExtra("personName", guide.getName() + " " + guide.getSurname()));
                        }else {
                            startActivity(new Intent(getContext(), ChatActivity.class).putExtra("personId",guide.getIdPerson())
                                    .putExtra("personName", guide.getName() + " " + guide.getSurname()));
                        }
                    }
                    else{
                        Toast.makeText(getContext(),
                                getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                    }
                    return true;
                }
            }).execute(new ServiceParams(getString(R.string.message_path) + client.getIdPerson() +
                    getString(R.string.message_retrieve_path) + guide.getIdPerson(), ServiceCaller.HTTP_GET, null));
        }
    };

}
