package hr.asc.snjezana.sightme.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by maja on 26.03.16..
 */
public class Place implements Serializable {

    long idPlace;
    String name;
    List<Person> person;

    public long getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(long idPlace) {
        this.idPlace = idPlace;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Person> getPerson() {
        return person;
    }

    public void setPerson(List<Person> person) {
        this.person = person;
    }
}