package hr.asc.snjezana.sightme.model;

import java.io.Serializable;

/**
 * Created by maja on 29.04.16..
 */
public class Messages implements Serializable {

    long idMessage;
    String message;
    String messageDate;
    String messageTime;
    boolean messageDeleted;
    Person from;
    Person to;

    public Messages() {
    }

    public Messages(String message, String messageDate, Person from) {
        this.message = message;
        this.messageDate = messageDate;
        this.from = from;
    }

    public long getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(long idMessage) {
        this.idMessage = idMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public boolean isMessageDeleted() {
        return messageDeleted;
    }

    public void setMessageDeleted(boolean messageDeleted) {
        this.messageDeleted = messageDeleted;
    }

    public Person getFrom() {
        return from;
    }

    public void setFrom(Person from) {
        this.from = from;
    }

    public Person getTo() {
        return to;
    }

    public void setTo(Person to) {
        this.to = to;
    }
}
