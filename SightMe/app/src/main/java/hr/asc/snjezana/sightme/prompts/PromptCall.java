package hr.asc.snjezana.sightme.prompts;

import android.app.Activity;

import hr.asc.snjezana.sightme.R;
import hr.asc.snjezana.sightme.core.ConnectionInterface;
import hr.asc.snjezana.sightme.core.ModularityCallback;

/**
 * Created by maja on 29.01.16..
 */
public class PromptCall implements ConnectionInterface {
    @Override
    public void connectUser(Activity jedan, ModularityCallback callback) {
        new InputPrompt(jedan).prepare(R.string.join_group, callback, R.string.join, R.string.cancel).showPrompt();
    }
}
