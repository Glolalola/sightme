package hr.asc.snjezana.sightme.stomp;
import java.util.Map;

/**
 * Created by maja on 07.05.16..
 */
public interface ListenerSubscription {
    void onMessage(Map<String, String> headers, String body);

}
