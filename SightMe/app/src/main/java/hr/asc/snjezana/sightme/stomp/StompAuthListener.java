package hr.asc.snjezana.sightme.stomp;

/**
 * Created by maja on 14.05.16..
 */
public interface StompAuthListener {

    void onAuthenticated();
}
