package hr.asc.snjezana.sightme;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.Arrays;
import java.util.List;

import hr.asc.snjezana.sightme.core.Input;
import hr.asc.snjezana.sightme.core.Logger;
import hr.asc.snjezana.sightme.handlers.RegistrationHandler;
import hr.asc.snjezana.sightme.model.Credentials;
import hr.asc.snjezana.sightme.model.Person;
import hr.asc.snjezana.sightme.webservice.ServiceAsyncTask;
import hr.asc.snjezana.sightme.webservice.ServiceCaller;
import hr.asc.snjezana.sightme.webservice.ServiceParams;

public class RegistrationActivity extends Activity {

    Button submit;
    EditText firstName;
    EditText lastName;
    EditText username;
    EditText password;
    EditText confirmPassword;
    Input passwordInput;
    Input confirmPasswordInput;
    Credentials credentials;
    RadioGroup radioGroup;
    RadioButton radioButton;
    List<Input> inputs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        // binding
        firstName = (EditText) findViewById(R.id.firstNameInput);
        lastName = (EditText) findViewById(R.id.lastNameInput);
        username = (EditText) findViewById(R.id.usernameInput);
        password = (EditText) findViewById(R.id.passwordInput);
        confirmPassword = (EditText) findViewById(R.id.confirmPasswordInput);
        radioGroup = (RadioGroup) findViewById(R.id.registrationRadioButton);
        submit = (Button) findViewById(R.id.submitButton);
        submit.setOnClickListener(onSubmit);



        // password inputs
        passwordInput = new Input(password, Input.PASSWORD_PATTERN, getString(R.string.password_error));
        confirmPasswordInput = new Input(confirmPassword,
                Input.PASSWORD_PATTERN, getString(R.string.confirm_password_error));

        // for validation
        inputs = Arrays.asList(
                new Input(firstName, Input.TEXT_MAIN_PATTERN, getString(R.string.first_name_error)),
                new Input(lastName, Input.TEXT_MAIN_PATTERN, getString(R.string.last_name_error)),
                new Input(username, Input.USERNAME_PATTERN, getString(R.string.username_error)),
                passwordInput,
                confirmPasswordInput
        );

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    /**
     * listener that triggers when submit is clicked
     */
    View.OnClickListener onSubmit = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Logger.log("RegistrationActivity -- initiated user registration");

            int selectedId = radioGroup.getCheckedRadioButtonId();
            radioButton = (RadioButton) findViewById(selectedId);
            String firstNameValue = firstName.getText().toString();
            String lastNameValue = lastName.getText().toString();
            String usernameValue = username.getText().toString();
            String passwordValue = password.getText().toString();
            String userType =  radioButton.getText().toString();


            if(Input.validate(inputs) && passwordInput.equals(confirmPasswordInput)){

                Logger.log("RegistrationActivity -- creating new user and sending info to service");

                credentials = new Credentials(usernameValue,passwordValue);
                Person person = new Person(0,firstNameValue,lastNameValue,credentials, userType);

                RegistrationHandler registrationHandler = new RegistrationHandler(RegistrationActivity.this, credentials);

                new ServiceAsyncTask(registrationHandler).execute(new ServiceParams(
                        getString(hr.asc.snjezana.sightme.R.string.person_signup_path),
                        ServiceCaller.HTTP_POST, person));
            }
        }
    };

}
