package hr.asc.snjezana.sightme.webservice;

/**
 * Created by maja on 29.04.16..
 */
public interface ServiceResponseHandler extends SimpleResponseHandler {

    void onPreSend();
    void onPostSend();

}
