package hr.asc.snjezana.sightme;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;

import hr.asc.snjezana.sightme.core.SessionManager;
import hr.asc.snjezana.sightme.model.Person;
import hr.asc.snjezana.sightme.model.Request;
import hr.asc.snjezana.sightme.model.RequestStatus;
import hr.asc.snjezana.sightme.webservice.ServiceAsyncTask;
import hr.asc.snjezana.sightme.webservice.ServiceCaller;
import hr.asc.snjezana.sightme.webservice.ServiceParams;
import hr.asc.snjezana.sightme.webservice.ServiceResponse;
import hr.asc.snjezana.sightme.webservice.SimpleResponseHandler;

public class RequestActivity extends AppCompatActivity {

    Button submit;
    EditText price;
    EditText description;
    EditText requestDate;
    EditText requstTime;
    Context context;

    public static final RequestStatus PENDING_STATUS = new RequestStatus(1, "pending");
    public static final RequestStatus ACCEPT_STATUS = new RequestStatus(2, "accept");
    public static final RequestStatus REJECTED_STATUS = new RequestStatus(3, "rejected");
    public static final RequestStatus PAID_STATUS = new RequestStatus(4, "payed");
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    private static final String CONFIG_CLIENT_ID = "AVFsxzT6_m_8dO2s286GZ5k5kRLS-YjAiHZkInqKx822Q__uUhMJqDYdXsEapxCiNWZPE4Qa0tYj8kwO";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID);
    Button payPal;
    Person client;
    Request r;

    public void sendResponse() {
        new ServiceAsyncTask(new SimpleResponseHandler() {
            @Override
            public boolean handleResponse(ServiceResponse response) {
                if(response.getHttpCode() == 200) {
                    Toast.makeText(context, "Operation successful", Toast.LENGTH_LONG).show();
                    finish();
                    return true;
                } else {
                    Toast.makeText(context, "Uh oh, something went wrong", Toast.LENGTH_LONG).show();
                    return false;
                }
            }
        }).execute(new ServiceParams(getString(R.string.request_answer_path), ServiceCaller.HTTP_POST, r));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);

        context = this;
        description = (EditText) findViewById(R.id.descriptionInput);
        requestDate = (EditText) findViewById(R.id.dateInput);
        requstTime = (EditText) findViewById(R.id.timeInput);
        price = (EditText) findViewById(R.id.priceInput);
        submit = (Button) findViewById(R.id.submitButton);
        submit.setOnClickListener(onSubmit);
        payPal = (Button) findViewById(R.id.payPal);
        client = SessionManager.getInstance(this).retrieveSession(SessionManager.PERSON_INFO_KEY, Person.class);

        r = new Gson().fromJson(getIntent().getExtras().getString("request"), Request.class);
        if(r != null) {
            description.setEnabled(false);
            description.setText(r.getDescription());
            requestDate.setEnabled(false);
            requestDate.setText(r.getRequestDate());
            requstTime.setEnabled(false);
            requstTime.setText(r.getRequestTime());
            price.setEnabled(false);
            price.setText(r.getPrice());

            Log.i("REQUEST", r.toString());
            if(r.getGuideStatus().getIdRequestStatus() == REJECTED_STATUS.getIdRequestStatus()) {
                payPal.setEnabled(false);
                submit.setEnabled(false);
                submit.setVisibility(View.GONE);
                payPal.setText("REJECTED");
            } else if(client.getUserType().equals("Guide")) {
                if(r.getGuideStatus().getIdRequestStatus() == PENDING_STATUS.getIdRequestStatus()) {
                    payPal.setText("Accept");
                    submit.setText("Reject");
                    payPal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            r.setTouristStatus(PENDING_STATUS);
                            r.setGuideStatus(ACCEPT_STATUS);
                            sendResponse();
                        }
                    });
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            r.setTouristStatus(REJECTED_STATUS);
                            r.setGuideStatus(REJECTED_STATUS);
                            sendResponse();
                        }
                    });
                } else {
                    payPal.setEnabled(false);
                    submit.setEnabled(false);
                    submit.setVisibility(View.GONE);
                    payPal.setText("ACCEPTED");
                }
            } else {
                submit.setVisibility(View.GONE);
                if(r.getGuideStatus().getIdRequestStatus() == ACCEPT_STATUS.getIdRequestStatus()) {
                    Intent intent = new Intent(this, PayPalService.class);
                    intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
                    startService(intent);
                    payPal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onBuyPressed();
                        }
                    });
                } else if (r.getTouristStatus().getIdRequestStatus() == PAID_STATUS.getIdRequestStatus()) {
                    payPal.setEnabled(false);
                    payPal.setText("PAID");
                } else {
                    payPal.setVisibility(View.GONE);
                }
            }
        } else {
            payPal.setVisibility(View.GONE);
        }
    }
    public void onBuyPressed() {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(RequestActivity.this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }
    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(r.getPrice()), "USD", r.getDescription(),
                paymentIntent);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.e("Show", confirm.toJSONObject().toString(4));
                        Log.e("Show", confirm.getPayment().toJSONObject().toString(4));

                        r.setTouristStatus(PAID_STATUS);
                        sendResponse();
                        Toast.makeText(getApplicationContext(), "PaymentConfirmation info received" +
                                " from PayPal", Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        Toast.makeText(getApplicationContext(), "an extremely unlikely failure" +
                                " occurred:", Toast.LENGTH_LONG).show();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "The user canceled.", Toast.LENGTH_LONG).show();
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Toast.makeText(getApplicationContext(), "An invalid Payment or PayPalConfiguration" +
                        " was submitted. Please see the docs.", Toast.LENGTH_LONG).show();
            }
        }
    }
    View.OnClickListener onSubmit = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            String descriptionValue = description.getText().toString();
            String dateValue = requestDate.getText().toString();
            String timeValue = requstTime.getText().toString();
            String priceValue = price.getText().toString();
            Person client = SessionManager.getInstance(getApplicationContext()).retrieveSession(SessionManager.PERSON_INFO_KEY, Person.class);
            Person guide = new Gson().fromJson(getIntent().getExtras().getString("guideId"), Person.class);
            Request r = new Request(0, priceValue, descriptionValue, dateValue, timeValue, false, guide, client, PENDING_STATUS, PENDING_STATUS);

            new ServiceAsyncTask(new SimpleResponseHandler() {
                @Override
                public boolean handleResponse(ServiceResponse response) {
                    finish();
                    Toast.makeText(getApplicationContext(), "Request successfuly sent", Toast.LENGTH_LONG).show();
                    return true;
                }
            }).execute(new ServiceParams(getString(R.string.request_create_path), ServiceCaller.HTTP_POST, r));
        }
    };
    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }
}
