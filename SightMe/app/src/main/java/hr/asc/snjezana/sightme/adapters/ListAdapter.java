package hr.asc.snjezana.sightme.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import hr.asc.snjezana.sightme.model.Messages;

/**
 * Created by maja on 26.03.16..
 */
public abstract class ListAdapter <T extends Serializable> extends ArrayAdapter<T> {

    private LayoutInflater inflater;
    private ArrayList<T> items;

    public ListAdapter(Context context, int resource, ArrayList<T> items) {
        super(context, resource);
        this.items = items;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public T getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getPosition(T item) {
        for(int i = 0; i < items.size(); i++) {
            if(items.get(i) == item) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public LayoutInflater getInflater() {
        return inflater;
    }

    public ArrayList<T> getItems() {
        return items;
    }

    @Override
    public void add(T object) {
        items.add(object);
        super.add(object);
    }
}
