package hr.asc.snjezana.sightme.stomp;

import android.util.Log;

import hr.asc.snjezana.sightme.core.Logger;
import hr.asc.snjezana.sightme.model.Person;
import hr.asc.snjezana.sightme.webservice.ServiceAsyncTask;
import hr.asc.snjezana.sightme.webservice.ServiceCaller;
import hr.asc.snjezana.sightme.webservice.ServiceParams;
import hr.asc.snjezana.sightme.webservice.ServiceResponse;
import hr.asc.snjezana.sightme.webservice.SimpleResponseHandler;


/**
 * Created by maja on 07.05.16..
 */
public class StompAuthentication {

    public String cookie;
    private StompAuthListener stompAuthListener;

    /**
     * authenticates the user for stomp transfer
     * @param client person that will be authenticated
     */
    public void authenticate(Person client) {
        new ServiceAsyncTask(new SimpleResponseHandler() {
            @Override
            public boolean handleResponse(ServiceResponse response) {
                Logger.log("Got response: " + response.toString(), getClass().getName(), Log.DEBUG);

                if (response.getHttpCode() == 202) {
                    cookie = response.getCookie();
                    if(stompAuthListener != null) stompAuthListener.onAuthenticated();
                    return true;
                } else {
                    Logger.log("Cookie fail", Log.WARN);
                    return false;
                }
            }
        }).execute(new ServiceParams(
                "/login",
                ServiceCaller.HTTP_POST, ServiceCaller.X_WWW_FORM_URLENCODED, null,
                "username=" + client.getCredentials().getUsername() +
                        "&password=" + client.getCredentials().getPassword()));
    }

    public String getCookie() {
        return cookie;
    }

    public void setStompAuthListener(StompAuthListener stompAuthListener) {
        this.stompAuthListener = stompAuthListener;
    }
}
