package hr.asc.snjezana.sightme;

import android.app.NotificationManager;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import hr.asc.snjezana.sightme.adapters.ChatArrayAdapter;
import hr.asc.snjezana.sightme.core.ListWrapper;
import hr.asc.snjezana.sightme.core.Logger;
import hr.asc.snjezana.sightme.core.SessionManager;
import hr.asc.snjezana.sightme.model.Messages;
import hr.asc.snjezana.sightme.model.Person;
import hr.asc.snjezana.sightme.stomp.ListenerSubscription;
import hr.asc.snjezana.sightme.stomp.Stomp;
import hr.asc.snjezana.sightme.stomp.StompAuthListener;
import hr.asc.snjezana.sightme.stomp.StompAuthentication;
import hr.asc.snjezana.sightme.stomp.StompSocket;
import hr.asc.snjezana.sightme.webservice.ServiceAsyncTask;
import hr.asc.snjezana.sightme.webservice.ServiceCaller;
import hr.asc.snjezana.sightme.webservice.ServiceParams;
import hr.asc.snjezana.sightme.webservice.ServiceResponse;
import hr.asc.snjezana.sightme.webservice.SimpleResponseHandler;

public class ChatActivity extends AppCompatActivity {

    private ChatArrayAdapter chatArrayAdapter;
    private ListView listView;
    private EditText chatText;
    private FloatingActionButton buttonSend;
    String title;
    ArrayList<Messages> mList;
    private long idPersonProfile;
    private Person client;
    private StompSocket socket;
    private StompAuthentication authentication;
    boolean viewing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        getWindow().setBackgroundDrawableResource(R.drawable.chatbcg) ;
        buttonSend = (FloatingActionButton) findViewById(R.id.send);
        listView = (ListView) findViewById(R.id.msgview);
        chatText = (EditText) findViewById(R.id.msg);

        // get current user
        client = SessionManager.getInstance(getApplicationContext())
                .retrieveSession(SessionManager.PERSON_INFO_KEY, Person.class);

        socket = new StompSocket();
        authentication = new StompAuthentication();
        authentication.setStompAuthListener(new StompAuthListener() {
            @Override
            public void onAuthenticated() {
                initiateStomp();
            }
        });
        authentication.authenticate(client);


        ListWrapper<Messages> wrapper = (ListWrapper<Messages>) getIntent().getSerializableExtra("messageList");
        if(wrapper != null) {
            mList = wrapper.getList();

        }
        idPersonProfile = getIntent().getLongExtra("personId", 0);
        title = getIntent().getStringExtra("personName");
        setTitle(title);

        chatArrayAdapter = new ChatArrayAdapter(this, R.layout.right, mList);
        listView.setAdapter(chatArrayAdapter);


        chatText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    sendChatMessage.onClick(v);
                    return true;
                }
                return false;
            }
        });
        buttonSend.setOnClickListener(sendChatMessage);

        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        //to scroll the list view to bottom on data change
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });
        viewing = true;
    }

    /**
     * initiates stomp by adding channels and subscribing
     */
    private void initiateStomp() {
        //new ServiceAsyncTask(null).execute(new ServiceParams(getString(R.string.message_path) + client.getCredentials().getUsername(), ServiceCaller.HTTP_POST, null));
        socket.addSubscriptionChannel(subscription, getString(R.string.group_channel) + client.getCredentials().getUsername());
        socket.subscribe(authentication.getCookie());
    }

    /**
     * stomp message callback
     */
    ListenerSubscription subscription = new ListenerSubscription() {
        @Override
        public void onMessage(Map<String, String> headers, String body) {
            Logger.log(body);

            if(body.equals(Stomp.SOCKET_FINISH)) {
                socket.close();
                return;
            }

            final Messages m = new Gson().fromJson(body, Messages.class);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    chatArrayAdapter.add(m);
                }
            });
        }
    };

    View.OnClickListener sendChatMessage = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Messages message = new Messages(chatText.getText().toString(),String.valueOf(new Date().getTime()), client);
            chatArrayAdapter.add(message);
            chatText.setText("");

            socket.send(getString(R.string.app_path) + getString(R.string.chat_path) + client.getIdPerson() + getString(R.string.message_save_path)
                    + idPersonProfile, message);
        }
    };

    @Override
    protected void onResume() {
        viewing = true;
        super.onResume();
    }

    @Override
    protected void onPause() {
        viewing = false;
        super.onPause();
    }
}
