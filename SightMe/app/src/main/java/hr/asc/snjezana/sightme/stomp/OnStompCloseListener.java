package hr.asc.snjezana.sightme.stomp;

/**
 * Created by maja on 07.05.16..
 */
public interface OnStompCloseListener {

    void onClose();

}
