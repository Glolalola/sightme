package hr.asc.snjezana.sightme.core;

import android.app.Activity;

/**
 * Created by maja on 29.01.16..
 */
public interface ConnectionInterface {
      void connectUser(Activity jedan, ModularityCallback callback);
}
