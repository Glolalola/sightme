package hr.asc.snjezana.sightme.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import hr.asc.snjezana.sightme.R;
import hr.asc.snjezana.sightme.model.Place;

/**
 * Created by maja on 26.03.16..
 */
public class PlaceAdapter extends ListAdapter<Place> {

    public PlaceAdapter(Context context, int resource, ArrayList<Place> items) {
        super(context, resource, items);
    }

    public static class ViewHolder {
        public TextView place;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;

        if (convertView == null) {
            vi = getInflater().inflate(R.layout.list_item_place,null);
            holder = new ViewHolder();


            holder.place = (TextView) vi.findViewById(R.id.itemPlace);


            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }

        Place current = getItems().get(position);
        holder.place.setText(current.getName());
        return vi;
    }


}

