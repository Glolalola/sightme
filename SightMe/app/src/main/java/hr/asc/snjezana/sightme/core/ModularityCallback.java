package hr.asc.snjezana.sightme.core;

/**
 * Created by maja on 29.01.16..
 */
public interface ModularityCallback {

    void onAction(String message);

}
