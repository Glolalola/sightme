package hr.asc.snjezana.sightme.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by maja on 29.04.16..
 */
public class RequestStatus implements Serializable {

    long idRequestStatus;
    String name;
    private List<Request> choosenGuideStatus;
    private List<Request> choosenTouristStatus;

    public RequestStatus(long idRequestStatus, String name) {
        this.idRequestStatus = idRequestStatus;
        this.name = name;
    }

    public long getIdRequestStatus() {
        return idRequestStatus;
    }

    public void setIdRequestStatus(long idRequestStatus) {
        this.idRequestStatus = idRequestStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Request> getChoosenGuideStatus() {
        return choosenGuideStatus;
    }

    public void setChoosenGuideStatus(List<Request> choosenGuideStatus) {
        this.choosenGuideStatus = choosenGuideStatus;
    }

    public List<Request> getChoosenTouristStatus() {
        return choosenTouristStatus;
    }

    public void setChoosenTouristStatus(List<Request> choosenTouristStatus) {
        this.choosenTouristStatus = choosenTouristStatus;
    }
}
