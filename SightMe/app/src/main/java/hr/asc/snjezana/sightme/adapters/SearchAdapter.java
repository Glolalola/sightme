package hr.asc.snjezana.sightme.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hr.asc.snjezana.sightme.R;
import hr.asc.snjezana.sightme.model.Person;

/**
 * Created by Glo on 14.5.2016..
 */
public class SearchAdapter extends CursorAdapter {


        ArrayList<Person> list;

        private TextView text;

        public SearchAdapter(Context context, Cursor cursor, ArrayList<Person> list) {

            super(context, cursor, false);

            this.list = list;

        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            //text.setText(list.get(cursor.getPosition()));

        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = inflater.inflate(R.layout.list_item_person, parent, false);

            text = (TextView) view.findViewById(R.id.text);

            return view;

        }

}

