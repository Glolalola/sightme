package hr.asc.snjezana.sightme.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import hr.asc.snjezana.sightme.R;
import hr.asc.snjezana.sightme.core.SessionManager;
import hr.asc.snjezana.sightme.model.Messages;
import hr.asc.snjezana.sightme.model.Person;

/**
 * Created by maja on 19.03.16..
 */
public class ChatArrayAdapter extends ListAdapter<Messages> {

    public ChatArrayAdapter(Context context, int textViewResourceId, ArrayList<Messages> messages) {
        super(context, textViewResourceId, messages);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Messages messagesObj = getItem(position);
        View row;
        Context context = getContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (messagesObj.getFrom().getCredentials().getUsername().equals(SessionManager.getInstance(context)
                .retrieveSession(SessionManager.PERSON_INFO_KEY, Person.class).getCredentials().getUsername())) {
            row = inflater.inflate(R.layout.right, parent, false);
        }else{
            row = inflater.inflate(R.layout.left, parent, false);
        }
        TextView chatText = (TextView) row.findViewById(R.id.msgr);
        chatText.setText(messagesObj.getMessage());
        return row;
    }
}
