package hr.asc.snjezana.sightme.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by maja on 26.03.16..
 */
public class Person implements Serializable {

    long idPerson;
    String name;
    String surname;
    Credentials credentials;
    String birthday;
    String userType;
    boolean active;
    String picture;

    private List<Messages> messageFrom;
    private List<Messages> messageTo;
    private List<Request> requestGuide;
    private List<Request> requestTourist;

    public Person() {
    }

    public Person(long idPerson, String name, String surname, Credentials credentials, String userType){
        this.idPerson = idPerson;
        this.name = name;
        this.surname = surname;
        this.credentials = credentials;
        this.userType = userType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(long idPerson) {
        this.idPerson = idPerson;
    }

    public List<Messages> getMessageFrom() {
        return messageFrom;
    }

    public void setMessageFrom(List<Messages> messageFrom) {
        this.messageFrom = messageFrom;
    }

    public List<Messages> getMessageTo() {
        return messageTo;
    }

    public void setMessageTo(List<Messages> messageTo) {
        this.messageTo = messageTo;
    }

    public List<Request> getRequestGuide() {
        return requestGuide;
    }

    public void setRequestGuide(List<Request> requestGuide) {
        this.requestGuide = requestGuide;
    }

    public List<Request> getRequestTourist() {
        return requestTourist;
    }

    public void setRequestTourist(List<Request> requestTourist) {
        this.requestTourist = requestTourist;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return this.name + " " + this.surname;
    }
}
