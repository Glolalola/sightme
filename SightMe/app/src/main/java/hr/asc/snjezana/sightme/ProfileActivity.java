package hr.asc.snjezana.sightme;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import hr.asc.snjezana.sightme.adapters.PlaceAdapter;
import hr.asc.snjezana.sightme.core.ListWrapper;
import hr.asc.snjezana.sightme.core.SessionManager;
import hr.asc.snjezana.sightme.model.Messages;
import hr.asc.snjezana.sightme.model.Person;
import hr.asc.snjezana.sightme.model.Place;
import hr.asc.snjezana.sightme.prompts.LoadingPrompt;
import hr.asc.snjezana.sightme.webservice.ServiceAsyncTask;
import hr.asc.snjezana.sightme.webservice.ServiceCaller;
import hr.asc.snjezana.sightme.webservice.ServiceParams;
import hr.asc.snjezana.sightme.webservice.ServiceResponse;
import hr.asc.snjezana.sightme.webservice.SimpleResponseHandler;

public class ProfileActivity extends AppCompatActivity {

    Button chat;
    Button request;
    PlaceAdapter adapter;
    ListView place;
    private long idPersonProfile;
    private Person client;
    TextView name;
    TextView surname;
    TextView birthday;
    Person userProfile;
    ArrayList<Messages> messageList;
    private Context context;
    CircleImageView mImageView;
    private ArrayList<Place> allPlaces;
    private ArrayList<Place> checkedPlaces;
    TextView places;

    private View.OnClickListener editPlaces = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            String[] placesStr = new String[allPlaces.size()];
            final boolean[] checked = new boolean[allPlaces.size()];
            for(int i = 0; i < placesStr.length; i++) {
                placesStr[i] = allPlaces.get(i).getName();
                checked[i] = false;
                if(checkedPlaces.contains(allPlaces.get(i))) {
                    checked[i] = true;
                }
            }
            builder.setMultiChoiceItems(placesStr, checked, new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                    checked[which] = isChecked;
                }
            });
            builder.setTitle("Choose places");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    checkedPlaces = new ArrayList<>();
                    for (int i = 0; i < checked.length; i++) {
                        if (checked[i]) {
                            checkedPlaces.add(allPlaces.get(i));
                        }
                    }
                    new ServiceAsyncTask(new SimpleResponseHandler() {
                        @Override
                        public boolean handleResponse(ServiceResponse response) {
                            if (response.getHttpCode() == 200) {
                                Toast.makeText(context, "Places successfuly checked", Toast.LENGTH_LONG).show();
                                PlaceAdapter adapter = new PlaceAdapter(context, R.layout.list_item_place, checkedPlaces);
                                place.setAdapter(adapter);
                                return true;
                            } else {
                                Toast.makeText(context, "Uh oh, something went wrong", Toast.LENGTH_LONG).show();
                                return false;
                            }
                        }
                    }).execute(new ServiceParams(getString(R.string.places_people_path) + client.getIdPerson(), ServiceCaller.HTTP_POST, checkedPlaces));
                }
            });
            builder.create().show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        context = this;
        name = (TextView) findViewById(R.id.profileName);
        surname = (TextView) findViewById(R.id.profileSurname);
        birthday = (TextView) findViewById(R.id.profileDate);
        place = (ListView) findViewById(R.id.place);
        chat =(Button)findViewById(R.id.btnMessage);
        request =(Button)findViewById(R.id.btnRequest);
        mImageView = (CircleImageView) findViewById(R.id.circleViewProfile);
        places = (TextView) findViewById(R.id.textPlaces);

        Bundle bundle  = getIntent().getExtras();
        userProfile = (Person)bundle.getSerializable("clientId");
        idPersonProfile = userProfile.getIdPerson();

        // all places
        new ServiceAsyncTask(new SimpleResponseHandler() {
            @Override
            public boolean handleResponse(ServiceResponse response) {
                Type listType = new TypeToken<ArrayList<Place>>(){}.getType();
                allPlaces = new Gson().fromJson(response.getJsonResponse(), listType);
                return true;
            }
        }).execute(new ServiceParams(getString(R.string.places_path), ServiceCaller.HTTP_GET, null));

        // get current user
        client = SessionManager.getInstance(getApplicationContext())
                .retrieveSession(SessionManager.PERSON_INFO_KEY, Person.class);

        chat.setOnClickListener(openChat);
        request.setOnClickListener(openRequest);
        getData();
        new ServiceAsyncTask(new SimpleResponseHandler() {
            @Override
            public boolean handleResponse(ServiceResponse response) {
                Type listType = new TypeToken<ArrayList<Place>>(){}.getType();
                checkedPlaces = new Gson().fromJson(response.getJsonResponse(), listType);
                PlaceAdapter adapter = new PlaceAdapter(context, R.layout.list_item_place, checkedPlaces);
                place.setAdapter(adapter);
                return true;
            }
        }).execute(new ServiceParams(getString(R.string.places_path) + idPersonProfile, ServiceCaller.HTTP_GET, null));
    }

    View.OnClickListener openChat = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final LoadingPrompt prompt = new LoadingPrompt(context);
            prompt.showPrompt();
            new ServiceAsyncTask(new SimpleResponseHandler() {
                @Override
                public boolean handleResponse(ServiceResponse response) {
                    prompt.hidePrompt();
                    if(response.getHttpCode() == 200) {

                        Type listType = new TypeToken<ArrayList<Messages>>(){}.getType();
                        messageList = new Gson().fromJson(response.getJsonResponse(), listType);

                        if(messageList != null){

                            startActivity(new Intent(getApplicationContext(), ChatActivity.class).putExtra("messageList",
                                    new ListWrapper<>(messageList)).putExtra("personId", idPersonProfile).putExtra("personName", userProfile.getName() + " " + userProfile.getSurname()));
                        }else {
                            startActivity(new Intent(getApplicationContext(), ChatActivity.class).putExtra("personId",idPersonProfile).putExtra("personName", userProfile.getName() + " " + userProfile.getSurname()));
                        }
                    }
                    else{
                        Toast.makeText(getApplicationContext(),
                                getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                    }
                    return true;
                }
            }).execute(new ServiceParams(getString(R.string.message_path) + client.getIdPerson() +
                    getString(R.string.message_retrieve_path) + idPersonProfile, ServiceCaller.HTTP_GET, null));
        }
    };

    View.OnClickListener openRequest = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(getApplicationContext(), RequestActivity.class);
            i.putExtra("guideId", new Gson().toJson(userProfile));
            startActivity(i);
        }
    };



    /**
     * called from outside activity to update person information
     * @param list list of persons online
     */
    public void updateList(ArrayList<Place> list){
        if(list != null) {
            adapter = new PlaceAdapter(this, R.layout.activity_profile, list);
            place.setAdapter(adapter);

        }
    }

    private void getData(){

        if(idPersonProfile == client.getIdPerson()){
            name.setText(client.getName());
            surname.setText(client.getSurname());
            birthday.setText(client.getBirthday());
            if(client.getPicture() == null) {
                Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.profile);
                Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 100);
                mImageView.setImageBitmap(circularBitmap);
            }

            chat.setVisibility(View.GONE);
            if(client.getUserType().equals("Guide")) {
                request.setText("Edit places");
                request.setOnClickListener(editPlaces);
                places.setVisibility(View.VISIBLE);
            } else {
                request.setVisibility(View.GONE);
                places.setVisibility(View.GONE);
            }
        } else {
            name.setText(userProfile.getName());
            surname.setText(userProfile.getSurname());
            birthday.setText(userProfile.getBirthday());
            if(userProfile.getPicture() == null) {
                Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.profile);
                Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 100);
                mImageView.setImageBitmap(circularBitmap);
            }
            chat.setVisibility(View.VISIBLE);
            request.setVisibility(View.VISIBLE);
        }
    }
}
