package hr.asc.snjezana.sightme;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import hr.asc.snjezana.sightme.adapters.ViewPageAdapter;
import hr.asc.snjezana.sightme.core.ListWrapper;
import hr.asc.snjezana.sightme.core.Logger;
import hr.asc.snjezana.sightme.core.SessionManager;
import hr.asc.snjezana.sightme.fragments.ContactsFragment;
import hr.asc.snjezana.sightme.fragments.RequestFragment;
import hr.asc.snjezana.sightme.model.Messages;
import hr.asc.snjezana.sightme.model.Person;
import hr.asc.snjezana.sightme.prompts.AlertPrompt;
import hr.asc.snjezana.sightme.stomp.ListenerSubscription;
import hr.asc.snjezana.sightme.stomp.Stomp;
import hr.asc.snjezana.sightme.stomp.StompAuthListener;
import hr.asc.snjezana.sightme.stomp.StompAuthentication;
import hr.asc.snjezana.sightme.stomp.StompSocket;
import hr.asc.snjezana.sightme.webservice.ServiceAsyncTask;
import hr.asc.snjezana.sightme.webservice.ServiceCaller;
import hr.asc.snjezana.sightme.webservice.ServiceParams;
import hr.asc.snjezana.sightme.webservice.ServiceResponse;
import hr.asc.snjezana.sightme.webservice.SimpleResponseHandler;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawer;
    private NavigationView navigationView;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Person client;
    public static ArrayList<Person> guideList;
    ArrayList<Person> personList;
    private StompSocket socket;
    private StompAuthentication authentication;
    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(R.layout.activity_main);

        guideList = new ArrayList<Person>();

        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.header,null, false);
        //mDrawer.addHeaderView(listHeaderView);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer,
                R.string.drawer_open, R.string.drawer_close);
        mDrawer.setDrawerListener(mDrawerToggle);

        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        TextView mUsernameView = (TextView) headerView.findViewById(R.id.headerName);
        TextView mEmailView = (TextView) headerView.findViewById(R.id.headerEmail);
        CircleImageView mImageView = (CircleImageView) headerView.findViewById(R.id.profile_image);

        //tabs
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        // get current user
        client = SessionManager.getInstance(getApplicationContext())
                .retrieveSession(SessionManager.PERSON_INFO_KEY, Person.class);

        // set credential on header
        mUsernameView.setText(client.toString());
        mEmailView.setText(client.getCredentials().getUsername());

        if(client.getUserType().equals("Guide")) {
            navigationView.getMenu().getItem(1).setVisible(false);
        }

        if(client.getPicture() == null) {
            Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.profile);
            Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 100);
            mImageView.setImageBitmap(circularBitmap);
        }

        // stomp
        socket = new StompSocket();
        authentication = new StompAuthentication();
        authentication.setStompAuthListener(new StompAuthListener() {
            @Override
            public void onAuthenticated() {
                initiateStomp();
            }
        });
        authentication.authenticate(client);

        getGuides();
    }

    /**
     * initiates stomp by adding channels and subscribing
     */
    private void initiateStomp() {
        //new ServiceAsyncTask(null).execute(new ServiceParams(getString(R.string.message_path) + client.getCredentials().getUsername(), ServiceCaller.HTTP_POST, null));
        socket.addSubscriptionChannel(subscription, getString(R.string.group_channel) + client.getCredentials().getUsername());
        socket.subscribe(authentication.getCookie());
    }

    /**
     * stomp message callback
     */
    ListenerSubscription subscription = new ListenerSubscription() {
        @Override
        public void onMessage(Map<String, String> headers, String body) {
            Logger.log(body);

            if(body.equals(Stomp.SOCKET_FINISH)) {
                socket.close();
                return;
            }

            final Messages m = new Gson().fromJson(body, Messages.class);
            // vibrate
            Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(1000);

            // issue notification
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                    .setSmallIcon(R.drawable.logo)
                    .setContentTitle("New message on SightMe")
                    .setContentText(m.getFrom().getName() + " " + m.getFrom().getSurname() + " says " + m.getMessage());

            int mNotificationId = 1;
            NotificationManager mNotifyMgr =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            mNotifyMgr.notify(mNotificationId, mBuilder.build());
        }
    };

    /**
     *  adding tab fragments
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        ViewPageAdapter adapter = new ViewPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new ContactsFragment(), getString(R.string.tabCon));
        adapter.addFragment(new RequestFragment(), getString(R.string.tabReq));
        viewPager.setAdapter(adapter);
    }
    /**
     * set items in menu
     * @param menuId
     */
    private void setNavigationMenuItems(int menuId) {

        navigationView.getMenu().clear();
        navigationView.inflateMenu(menuId);
    }
    /**
     * ask for sign out
     */
    private void signOut() {
        DialogInterface.OnClickListener signOutListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SessionManager.getInstance(getApplicationContext()).destroyAll();
                dialog.dismiss();
                //if (socket != null) socket.close();
                for (int i = 0; i < getFragmentManager().getBackStackEntryCount(); ++i) {
                    getFragmentManager().popBackStack();
                }
                MainActivity.super.onBackPressed();
            }
        };
        AlertPrompt signOutPrompt = new AlertPrompt(this);
        signOutPrompt.prepare(R.string.log_out_question, signOutListener,
                R.string.log_out, null, R.string.cancel);
        signOutPrompt.showPrompt();
    }


    @Override
    public void onBackPressed() {
        mDrawer.closeDrawers();
        signOut();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(mDrawer.isDrawerOpen(navigationView)){
            mDrawer.closeDrawers();
        }else {
            mDrawer.openDrawer(navigationView);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        // handling navigation items
        if (menuItem.getItemId() == R.id.profile) {
            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
            Bundle b = new Bundle();
            b.putSerializable("clientId",client);
            intent.putExtras(b);
            startActivity(intent);
        }else if(menuItem.getItemId() == R.id.guide_list){
            startActivity(new Intent(getApplicationContext(), GuideListActivity.class).putExtra("guideList",
                    new ListWrapper<>(guideList)));
        } else if (menuItem.getItemId() == R.id.logout) {
            signOut();
        }

        mDrawer.closeDrawers();
        return true;
    }

    public void getGuides (){
        new ServiceAsyncTask(new SimpleResponseHandler() {
            @Override
            public boolean handleResponse(ServiceResponse response) {
                if(response.getHttpCode() == 200) {

                    Type listType = new TypeToken<ArrayList<Person>>(){}.getType();
                    personList = new Gson().fromJson(response.getJsonResponse(), listType);

                    for(Person p: personList){
                        if(p.getUserType().equals("Guide")){
                            MainActivity.guideList.add(p);
                        }
                    }
                }
                else{
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                }
                return true;
            }
        }).execute(new ServiceParams(getString(hr.asc.snjezana.sightme.R.string.person_path),
                ServiceCaller.HTTP_GET, null));

    }
}
