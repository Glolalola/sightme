package hr.asc.snjezana.sightme.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import hr.asc.snjezana.sightme.R;
import hr.asc.snjezana.sightme.core.Logger;
import hr.asc.snjezana.sightme.model.Request;

/**
 * Created by maja on 26.03.16..
 */
public class RequestAdapter extends  ListAdapter<Request>{



    public RequestAdapter(Context context, int resource, ArrayList<Request> items) {
        super(context, resource, items);
    }


    public static class ViewHolder {
        public TextView requestName;
        public TextView requestStatus;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;
        try {
            if (convertView == null) {
                vi = getInflater().inflate(R.layout.list_item_request, null);
                holder = new ViewHolder();
                holder.requestName = (TextView) vi.findViewById(R.id.name);
                holder.requestStatus = (TextView) vi.findViewById(R.id.status);

                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }

            Request current = getItems().get(position);
            holder.requestName.setText(current.getGuide().toString());
            holder.requestStatus.setText(current.getGuideStatus().getName().toUpperCase());

            switch(holder.requestStatus.getText().toString()) {
                case "PENDING":
                    holder.requestStatus.setTextColor(Color.parseColor("#ffae00"));
                    break;
                case "ACCEPTED":
                    holder.requestStatus.setTextColor(Color.GREEN);
                    break;
                case "PAID":
                    holder.requestStatus.setTextColor(Color.GREEN);
                    break;
                case "REJECTED":
                    holder.requestStatus.setTextColor(Color.RED);
                    break;
            }

        } catch (Exception e) {
            Logger.log("Failed to fill view with requests", getClass().getName(), Log.ERROR);
        }
        return vi;
    }
}
