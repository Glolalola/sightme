package hr.asc.snjezana.sightme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import hr.asc.snjezana.sightme.adapters.PersonAdapter;
import hr.asc.snjezana.sightme.core.ListWrapper;
import hr.asc.snjezana.sightme.model.Person;

public class GuideListActivity extends AppCompatActivity {

    PersonAdapter adapter;
    ListView users;
    ArrayList<Person> list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_list);
        users = (ListView) findViewById(R.id.guides);
        list = new ArrayList<>();
        list.addAll(((ListWrapper) getIntent().getSerializableExtra("guideList")).getList());
        updateList(list);
        users.setOnItemClickListener(onItemClickListener);
    }
    

    /**
     * called from outside activity to update the list of persons
     * @param list list of persons online
     */
    public void updateList(ArrayList<Person> list){
        if(list != null) {
            adapter = new PersonAdapter(this, R.layout.activity_guide_list, list);
            users.setAdapter(adapter);

        }
    }

    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Person guide = (Person) users.getItemAtPosition(position);

            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
            Bundle b = new Bundle();
            b.putSerializable("clientId",guide);
            intent.putExtras(b);
            startActivity(intent);
        }
    };

}
