/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.asc.snjezana.sightme.controller;

import hr.asc.snjezana.sightme.model.Person;
import hr.asc.snjezana.sightme.model.Places;
import hr.asc.snjezana.sightme.repositories.PersonRepository;
import hr.asc.snjezana.sightme.repositories.PlacesRepository;
import java.util.List;
import java.util.Set;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author maja
 */
@RestController
@RequestMapping(value = "/places")
public class PlacesController {
    PersonRepository personRepository;
    PlacesRepository placesRepository;
    
    @Autowired

    public PlacesController(PersonRepository personRepository, PlacesRepository placesRepository) {
        this.personRepository = personRepository;
        this.placesRepository = placesRepository;
    }
    
    @RequestMapping(value = "/")
    public ResponseEntity<List<Person>> retrieveAll() {
        Logger.getLogger("PersonController.java").log(Logger.Level.INFO,
                "/person -- retrieving full list of places");
        return new ResponseEntity(this.placesRepository.findAll(), HttpStatus.OK);
    }
    
    @RequestMapping("/{idPerson}")
    public ResponseEntity<Set<Places>> getPlaces(@PathVariable long idPerson){
        Person person = this.personRepository.findByIdPerson(idPerson);
        
        return new ResponseEntity(person.getPlace(), HttpStatus.OK);
    }
    
    @RequestMapping("/map/{idPerson}")
    public ResponseEntity createPlaces(@PathVariable long idPerson, @RequestBody Set<Places> places) {
        Person person = this.personRepository.findByIdPerson(idPerson);
        person.setPlace(places);
        this.personRepository.save(person);
        return new ResponseEntity(HttpStatus.OK);
    }
    
}
