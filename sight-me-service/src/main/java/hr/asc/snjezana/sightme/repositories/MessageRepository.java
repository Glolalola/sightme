/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.asc.snjezana.sightme.repositories;

import hr.asc.snjezana.sightme.model.Message;
import hr.asc.snjezana.sightme.model.Person;
import java.util.List;
import javax.persistence.Table;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author maja
 */

@Repository
@Table(name="messages")
public interface MessageRepository extends JpaRepository<Message, String> {
    public Message findByIdMessage( long idMessage);
    /*
    findDistinctPeopleByLastnameOrFirstname
            queryFirst10ByLastname
*/
    public List<Message> findByTo (Person to);
    public List<Message> findByFrom(Person from);
    
    @Query(value = "SELECT * FROM messages WHERE message_from = ?1 AND message_to = ?2 OR message_from = ?2 AND message_to = ?1", nativeQuery = true)
    public List<Message> findMessages(long idTo, long idFrom);
    //public Message findByAnnotatedQuery(String param);
    public List<Message> findFirst10ByToAndFromOrderByMessageDate(Person to, Person from);
}
