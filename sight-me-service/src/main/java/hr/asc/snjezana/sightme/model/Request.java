/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.asc.snjezana.sightme.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author maja
 */
@Entity
@Table(name="request")
public class Request implements Serializable{
    
    @Id 
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_request")
    long idRequest;
    
    @Column(name="price")
    String price;
    
    @Column(name="description")
    String description;
    
    @Column(name="request_date")
    String requestDate;
    
    @Column(name="request_time")
    String requestTime;
    
    @Column(name="deleted")
    boolean requestDeleted;
    
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "guide")
    private Person guide;
    
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "tourist")
    private Person tourist;
    
    @ManyToOne
    @JoinColumn(name = "guide_status")
    private RequestStatus guideStatus;
    
    @ManyToOne
    @JoinColumn(name = "tourist_status")
    private RequestStatus touristStatus;

    public Request() {
    }

    public Request(long idRequest, String price, String description, String requestDate, String requestTime, Person guide, Person tourist, RequestStatus guideStatus, RequestStatus touristStatus) {
        this.idRequest = idRequest;
        this.price = price;
        this.description = description;
        this.requestDate = requestDate;
        this.requestTime = requestTime;
        this.guide = guide;
        this.tourist = tourist;
        this.guideStatus = guideStatus;
        this.touristStatus = touristStatus;
    }

    public long getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(long idRequest) {
        this.idRequest = idRequest;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public boolean isRequestdeleted() {
        return requestDeleted;
    }

    public void setRequestdeleted(boolean requestDeleted) {
        this.requestDeleted = requestDeleted;
    }

    public Person getGuide() {
        return guide;
    }

    public void setGuide(Person guide) {
        this.guide = guide;
    }

    public Person getTourist() {
        return tourist;
    }

    public void setTourist(Person tourist) {
        this.tourist = tourist;
    }

    public RequestStatus getGuideStatus() {
        return guideStatus;
    }

    public void setGuideStatus(RequestStatus guideStatus) {
        this.guideStatus = guideStatus;
    }

    public RequestStatus getTouristStatus() {
        return touristStatus;
    }

    public void setTouristStatus(RequestStatus touristStatus) {
        this.touristStatus = touristStatus;
    }
    
    
}
