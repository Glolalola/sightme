/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.asc.snjezana.sightme.repositories;

import hr.asc.snjezana.sightme.model.Person;
import hr.asc.snjezana.sightme.model.Request;
import java.util.List;
import javax.persistence.Table;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author maja
 */
@Repository
@Table(name="request")
public interface RequestRepository extends JpaRepository<Request, String>{
    public Request findByIdRequest( long idRequest);
    public List<Request> findByTourist(Person tourist);
    public List<Request> findByGuide(Person guide);
}
