/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.asc.snjezana.sightme.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author maja
 */
@Entity
@Table(name="person")
public class Person implements Serializable{
    
    @Id 
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_user")
    long idPerson;
    
    @Column(name="name")
    String name;
    
    @Column(name="surname")
    String surname;
    
    @Embedded
    Credentials credentials;
    
    @Column(name="birth_date")
    String birthday;
    
    @Column(name="user_type")
    String userType;
    
    @Column(name="active")
    boolean active;
    
    @Column(name="picture")
    String picture;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "person_has_places", joinColumns = { @JoinColumn(name = "id_user") },
            inverseJoinColumns = { @JoinColumn(name = "id_place") })
    Set<Places> place = new HashSet<>();
    
    @JsonIgnore
    @OneToMany(mappedBy = "from", fetch = FetchType.LAZY)
    private List<Message> messageFrom;
    
    @JsonIgnore
    @OneToMany(mappedBy = "to", fetch = FetchType.LAZY)
    private List<Message> messageTo;
    
    @JsonIgnore
    @OneToMany(mappedBy = "guide", fetch = FetchType.LAZY)
    private List<Request> requestGuide;
    
    @JsonIgnore
    @OneToMany(mappedBy = "tourist", fetch = FetchType.LAZY)
    private List<Request> requestTourist;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "person_role", joinColumns = { @JoinColumn(name = "id_person") },
            inverseJoinColumns = { @JoinColumn(name = "id_role") })
    Set<Role> roles = new HashSet<>();

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Person() {
    }
    
    public Person(Person person){
        super();
        this.idPerson = person.getIdPerson();
        this.name = person.getName();
        this.surname = person.getSurname();
        this.credentials = person.getCredentials();
        this.userType = person.getUserType();
    }

    public Person(long idPerson, String name, String surname, Credentials credentials, String birthday, String userType) {
        this.idPerson = idPerson;
        this.name = name;
        this.surname = surname;
        this.credentials = credentials;
        this.birthday = birthday;
        this.userType = userType;
    }

   

    public long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(long idPerson) {
        this.idPerson = idPerson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Set<Places> getPlace() {
        return place;
    }

    public void setPlace(Set<Places> place) {
        this.place = place;
    }

    public List<Message> getMessageFrom() {
        return messageFrom;
    }

    public void setMessageFrom(List<Message> messageFrom) {
        this.messageFrom = messageFrom;
    }

    public List<Message> getMessageTo() {
        return messageTo;
    }

    public void setMessageTo(List<Message> messageTo) {
        this.messageTo = messageTo;
    }

    public List<Request> getRequestGuide() {
        return requestGuide;
    }

    public void setRequestGuide(List<Request> requestGuide) {
        this.requestGuide = requestGuide;
    }

    public List<Request> getRequestTourist() {
        return requestTourist;
    }

    public void setRequestTourist(List<Request> requestTourist) {
        this.requestTourist = requestTourist;
    }
 
}
