/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.asc.snjezana.sightme.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author maja
 */
@Entity
@Table(name="request_status")
public class RequestStatus implements Serializable{
    
    @Id 
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_request_status")
    long idRequestStatus;
    
    @Column(name="name")
    String name;
    
    @JsonIgnore
    @OneToMany(mappedBy = "guideStatus", fetch = FetchType.LAZY)
    private List<Request> choosenGuideStatus;
    
    @JsonIgnore
    @OneToMany(mappedBy = "touristStatus", fetch = FetchType.LAZY)
    private List<Request> choosenTouristStatus;

    public RequestStatus() {
    }

    public RequestStatus(long idRequestStatus, String name, List<Request> choosenGuideStatus, List<Request> choosenTouristStatus) {
        this.idRequestStatus = idRequestStatus;
        this.name = name;
        this.choosenGuideStatus = choosenGuideStatus;
        this.choosenTouristStatus = choosenTouristStatus;
    }

    public long getIdRequestStatus() {
        return idRequestStatus;
    }

    public void setIdRequestStatus(long idRequestStatus) {
        this.idRequestStatus = idRequestStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Request> getChoosenGuideStatus() {
        return choosenGuideStatus;
    }

    public void setChoosenGuideStatus(List<Request> choosenGuideStatus) {
        this.choosenGuideStatus = choosenGuideStatus;
    }

    public List<Request> getChoosenTouristStatus() {
        return choosenTouristStatus;
    }

    public void setChoosenTouristStatus(List<Request> choosenTouristStatus) {
        this.choosenTouristStatus = choosenTouristStatus;
    }
    
    
}
