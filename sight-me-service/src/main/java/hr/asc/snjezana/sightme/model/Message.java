/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.asc.snjezana.sightme.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author maja
 */
@Entity
@Table(name="messages")
public class Message implements Serializable{
    
    @Id 
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id_messages")
    long idMessage;
    
    @Column(name="message")
    String message;
    
    @Column(name="message_date")
    String messageDate;
    
    @Column(name="message_time")
    String messageTime;
    
    @Column(name="deleted")
    boolean messageDeleted;
    
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "message_from")
    private Person from;
    
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "message_to")
    private Person to;

    public Message() {
    }

    public Message(long idMessage, String message, String messageDate, String messageTime, Person from, Person to) {
        this.idMessage = idMessage;
        this.message = message;
        this.messageDate = messageDate;
        this.messageTime = messageTime;
        this.from = from;
        this.to = to;
    }

    
    
    public long getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(long idMessage) {
        this.idMessage = idMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public boolean isMessageDeleted() {
        return messageDeleted;
    }

    public void setMessageDeleted(boolean messageDeleted) {
        this.messageDeleted = messageDeleted;
    }

    public Person getFrom() {
        return from;
    }

    public void setFrom(Person from) {
        this.from = from;
    }

    public Person getTo() {
        return to;
    }

    public void setTo(Person to) {
        this.to = to;
    }
    
    
}
