/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.asc.snjezana.sightme.configuration;

import java.security.Principal;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.context.annotation.*;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;

import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.*;
import org.springframework.web.socket.messaging.SessionConnectEvent;

/**
 *
 * @author paz
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer{
    
    private Object SimpMessageHeaderAccessor;

    public WebSocketConfig() {
        
    }
  
    
  
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/queue", "/topic" );
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
      registry.addEndpoint("/chat");

    }
  
    @Override
    public void configureClientInboundChannel(ChannelRegistration channelRegistration) {

    }

    @Override
    public void configureClientOutboundChannel(ChannelRegistration channelRegistration) {

    }

    
    /**
    *
    * @param converters
    * @return
    */
    
    @Override
    public boolean configureMessageConverters(List<MessageConverter> converters) {
        /*
        converters.add((MessageConverter) new FormHttpMessageConverter());
        converters.add((MessageConverter) new StringHttpMessageConverter());
        converters.add((MessageConverter) new MappingJackson2HttpMessageConverter());
                */
      return true;
    }

  
    @EventListener    
    private void handleSessionConnected(SessionConnectEvent event) { 
        SimpMessageHeaderAccessor headers;

        Principal a =  event.getUser();
        String username = a.getName();

        Logger.getLogger("WebSocketConfig.java").log(Logger.Level.INFO,
        "Created session for " + username);
    }
}
