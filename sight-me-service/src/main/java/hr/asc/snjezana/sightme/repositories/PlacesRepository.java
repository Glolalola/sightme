/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.asc.snjezana.sightme.repositories;

import hr.asc.snjezana.sightme.model.Places;
import java.io.Serializable;
import javax.persistence.Table;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author maja
 */
@Repository
@Table(name="person_has_places")
public interface PlacesRepository extends CrudRepository<Places, String> {
    
}
