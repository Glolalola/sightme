/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.asc.snjezana.sightme.controller;

import hr.asc.snjezana.sightme.model.Message;
import hr.asc.snjezana.sightme.model.Person;
import hr.asc.snjezana.sightme.repositories.PersonRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.ArrayList;
import hr.asc.snjezana.sightme.repositories.MessageRepository;
import java.util.HashSet;
import java.util.Set;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author maja
 */
@RestController
@RequestMapping(value = "/message")
public class MessageController {
    MessageRepository  messageRepository;
    PersonRepository personRepository;

    @Autowired
    public MessageController(MessageRepository messageRepository, PersonRepository personRepository) {
        this.messageRepository = messageRepository;
        this.personRepository = personRepository;
    }
    
    /**
     * 
     * @param idPerson
     * @return 
     */
    @RequestMapping("/contacts/{idPerson}")
    public ResponseEntity<Set<Person>> getContacts(@PathVariable long idPerson) {
        Person person = this.personRepository.findByIdPerson(idPerson);
        List<Message> messagesTo = this.messageRepository.findByTo(person);
        List<Message> messagesFrom = this.messageRepository.findByFrom(person);
        Set<Person> contacts = new HashSet<>();
        for(Message m : messagesTo) {
            System.out.println("Found contact " + m.getFrom().getName());
            contacts.add(m.getFrom());
        }
        for(Message m : messagesFrom) {
            System.out.println("Found contact " + m.getTo().getName());
            contacts.add(m.getTo());
        }
        return new ResponseEntity(contacts, HttpStatus.OK);
    }
    
    /**
     * retrieves all messages
     * @param idOwner
     * @param idPerson 
     */
    
    @RequestMapping("/{idOwner}/get/{idPerson}")
    public ResponseEntity<List<Message>> retrieveMessages(@PathVariable long idOwner, @PathVariable long idPerson){
        //List<Message> messagesTo = this.messageRepository.findFirst10ByToAndFromOrderByMessageDate(to, from);
        //List<Message> messagesFrom = this.messageRepository.findFirst10ByToAndFromOrderByMessageDate(from, to);
        List<Message> messages = this.messageRepository.findMessages(idOwner, idPerson);
        /*int i = 0, j = 0;
        System.out.println("to size: " + messagesTo.size());
        System.out.println("from size: " + messagesFrom.size());
        while(i < messagesTo.size() || j < messagesFrom.size()) {
            System.out.println("i: " + i);
            System.out.println("j: " + j);
            if(i >= messagesTo.size()) {
                System.out.println("i finish");
                if(j < messagesFrom.size()) {
                    messages.addAll(messagesFrom.subList(j, messagesFrom.size()));
                }
                break;
            }
            if(j >= messagesFrom.size()) {
                System.out.println("j finish");
                if(i < messagesTo.size()) {
                    messages.addAll(messagesTo.subList(i, messagesTo.size()));
                }
                break;
            }
            
            System.out.println("standard");
            long messagesToTime = Long.valueOf(messagesTo.get(i).getMessageDate());
            long messagesFromTime = Long.valueOf(messagesFrom.get(j).getMessageDate());
            
            if(messagesToTime < messagesFromTime || messagesToTime == messagesFromTime) {
                messages.add(messagesTo.get(i));
                i++;
            } else {
                messages.add(messagesFrom.get(j));
                j++;
            }
        }*/
        return new ResponseEntity(messages, HttpStatus.OK);
    }
    
}
