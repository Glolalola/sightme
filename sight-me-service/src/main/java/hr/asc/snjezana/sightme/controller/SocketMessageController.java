/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.asc.snjezana.sightme.controller;

import hr.asc.snjezana.sightme.model.Message;
import hr.asc.snjezana.sightme.model.Person;
import hr.asc.snjezana.sightme.repositories.MessageRepository;
import hr.asc.snjezana.sightme.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

/**
 *
 * @author maja
 */
@Controller
public class SocketMessageController {
    
    private SimpMessagingTemplate template;
    MessageRepository  messageRepository;
    PersonRepository personRepository;

    @Autowired
    public SocketMessageController(SimpMessagingTemplate template,MessageRepository messageRepository, PersonRepository personRepository) {
        this.template = template;
        this.messageRepository = messageRepository;
        this.personRepository = personRepository;
    }
    
    @MessageMapping("/chat/{idOwner}/save/{idPerson}")
    public void saveMessage(@Payload Message message, @DestinationVariable long idOwner, @DestinationVariable long idPerson) {
        System.out.println("MESSAGE");
        Person from = this.personRepository.findByIdPerson(idOwner);
        Person to = this.personRepository.findByIdPerson(idPerson);
        message.setFrom(from);
        message.setTo(to);
        this.messageRepository.save(message);
        template.convertAndSend("/topic/message/" + to.getCredentials().getUsername(), message);
    }
}
