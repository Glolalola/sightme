/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.asc.snjezana.sightme.controller;

import hr.asc.snjezana.sightme.model.Person;
import hr.asc.snjezana.sightme.model.Request;
import hr.asc.snjezana.sightme.repositories.PersonRepository;
import hr.asc.snjezana.sightme.repositories.RequestRepository;
import java.util.List;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author maja
 */
@RestController
@RequestMapping(value = "/request")
public class RequestController {
    
    RequestRepository requestRepository;
    PersonRepository personRepository;
    
    @Autowired
    public RequestController(RequestRepository requestRepository, PersonRepository personRepository) {
        
        this.requestRepository = requestRepository;
        this.personRepository = personRepository;
    }
    
    /**
     * gets all requests from database
     * @return all groups in json format with HTTP 200
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<Request>> retrieveAll() {
        Logger.getLogger("RequestController.java").log(Logger.Level.INFO,
                "GET on /request/ -- retrieving full list of requests");

        return new ResponseEntity(this.requestRepository.findAll(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/{id}")
    public ResponseEntity<Request> retrieveById(@RequestParam long idRequest) {
        Request r = this.requestRepository.findByIdRequest(idRequest);
        if(r != null) {
            return new ResponseEntity(r, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(value = "/answer")
    public ResponseEntity doSomething(@RequestBody Request request) {
        this.requestRepository.save(request);
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value = "/person")
    public ResponseEntity<List<Request>> retrieveByPerson(@RequestBody Person p) {
        List<Request> r;
        if(p.getUserType().equals("Tourist")) {
            r = this.requestRepository.findByTourist(p);
        } else {
            r = this.requestRepository.findByGuide(p);
        }
        if(r != null) {
            return new ResponseEntity(r, HttpStatus.OK);
        } else {
            return new ResponseEntity(r, HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(value = "/create")
    public ResponseEntity saveRequest(@RequestBody Request r) {
        Request req = this.requestRepository.save(r);
        if(req != null) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
